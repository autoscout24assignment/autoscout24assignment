package com.assignment.data.mapper

interface BaseMapper<E, D> {

    fun transformToEntity(type: E): D

}