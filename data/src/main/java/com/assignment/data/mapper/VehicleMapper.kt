package com.assignment.data.mapper

import com.assignment.data.datasource.remote.response.dto.VehicleDto
import com.assignment.domain.entity.Vehicle

open class VehicleMapper : BaseMapper<VehicleDto, Vehicle> {
    override fun transformToEntity(vehicle: VehicleDto): Vehicle {
        return Vehicle(
            id = vehicle.id,
            title = vehicle.make.orEmpty()+" "+vehicle.model.orEmpty(),
            fuelType = vehicle.fuel.orEmpty(),
            price = if (vehicle.price != null) {
                vehicle.price.toString()
            } else "",
            comment = vehicle.comment.orEmpty(),
            images = if (vehicle.images != null) {
                vehicle.images.map { it.url }
            } else arrayListOf()
        )
    }
}