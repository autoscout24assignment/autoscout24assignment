package com.assignment.data.repository.base

import android.util.Log
import com.assignment.data.coroutine.DefaultCoroutineImpl
import com.assignment.domain.util.ResultState
import com.datasolutions.data.coroutine.DefaultCorotuine
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.net.SocketTimeoutException
import java.net.UnknownHostException


abstract class BaseRepoImpl {

    protected suspend fun <T : Any> safeApiCall(apiCall: suspend () -> T): ResultState<T> {
        return try {
            val response = apiCall.invoke()
            if (response != null)
                ResultState.Success(response)
            else
                ResultState.Error("Opps, Something went wrong. Please try again")
        } catch (throwable: Throwable) {
            when (throwable) {
                is HttpException -> {
                    val result = when (throwable.code()) {
                        in 500..599 -> {
                            ResultState.Error("Server error.")
                        }
                        in 400..451 -> {
                            ResultState.Error("Http error.")
                        }
                        else -> {
                            ResultState.Error("Unknown error.")
                        }
                    }
                    result
                }

                is UnknownHostException -> ResultState.Error("Unable to resolve host.")
                is SocketTimeoutException -> ResultState.Error("Slow internet connection.")
                else -> ResultState.Error(throwable.message.orEmpty())
            }

        }
    }

}