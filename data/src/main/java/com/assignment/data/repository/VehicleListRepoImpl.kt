package com.assignment.data.repository

import com.assignment.data.repository.base.BaseRepoImpl
import com.assignment.domain.entity.Vehicle
import com.assignment.domain.repository.VehicleListRepo
import com.assignment.domain.util.ResultState
import com.assignment.data.datasource.remote.ApiService
import com.assignment.data.datasource.remote.response.dto.VehicleDto
import com.assignment.data.datasource.remote.response.dto.VehicleNoteDto
import com.assignment.data.mapper.VehicleMapper

import com.datasolutions.data.coroutine.DefaultCorotuine
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.*


class VehicleListRepoImpl(
    private val service: ApiService,
    private val coroutineContext: DefaultCorotuine,
    private val mapper: VehicleMapper
) :
    BaseRepoImpl(),
    VehicleListRepo {

    override suspend fun getVehicles(): Flow<ResultState<List<Vehicle>>> =
        flow {
            emit(ResultState.loading)
            coroutineScope {
                val vehicles = async { safeApiCall { service.getVehicles() } }
                val vehiclesNotes = async { safeApiCall { service.getVehiclesNotes() } }
                val vehiclesWithNotes =
                    checkForVehicleComments(vehicles.await(), vehiclesNotes.await())
                if (vehiclesWithNotes is ResultState.Success) {
                    emit(ResultState.Success(vehiclesWithNotes.data.map {
                        mapper.transformToEntity(it)
                    }))
                } else if (vehiclesWithNotes is ResultState.Error) {
                    emit(vehiclesWithNotes)
                }
            }

        }.flowOn(coroutineContext.IO())

    private fun checkForVehicleComments(
        vehicles: ResultState<List<VehicleDto>>,
        vehicleNotes: ResultState<List<VehicleNoteDto>>
    ): ResultState<List<VehicleDto>> {

        return when (vehicles) {
            is ResultState.Success -> {
                if (vehicleNotes is ResultState.Success) {
                    vehicles.data.map { vehicle ->
                        vehicle.comment =
                            vehicleNotes.data.find { vehicleNoteDto -> vehicleNoteDto.vehicleId == vehicle.id }?.note
                    }
                }
                vehicles
            }
            else -> {
                vehicles
            }
        }
    }
}