package com.assignment.data.config

object Configuration {

    const val VEHICLE_LIST_URL = "http://private-fe87c-simpleclassifieds.apiary-mock.com/"
    const val VEHICLE_NOTES_URL = "https://private-e7c3d8-classifiednotes.apiary-mock.com/"
}