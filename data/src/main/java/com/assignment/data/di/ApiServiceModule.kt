package com.assignment.data.di


import com.assignment.data.datasource.remote.ApiService
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module
import retrofit2.Retrofit

object ApiServiceModule {

    fun load() {
        loadKoinModules(apiService)
    }

    private val apiService = module {

        single { get<Retrofit>().create(ApiService::class.java) }
    }
}