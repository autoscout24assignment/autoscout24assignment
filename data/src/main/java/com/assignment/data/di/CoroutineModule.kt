package com.assignment.data.di

import com.assignment.data.coroutine.DefaultCoroutineImpl
import com.datasolutions.data.coroutine.DefaultCorotuine
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module

object CoroutineModule {

    fun load() {
        loadKoinModules(coroutineModule)
    }

    private val coroutineModule = module {
        single<DefaultCorotuine> { DefaultCoroutineImpl() }
    }
}