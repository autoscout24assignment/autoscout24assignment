package com.assignment.data.di

import com.assignment.data.mapper.VehicleMapper
import com.assignment.data.repository.VehicleListRepoImpl
import com.assignment.domain.repository.VehicleListRepo
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module

object RepositoryModule {

    fun load() {
        loadKoinModules(repository)
    }

    private val repository = module {
        factory<VehicleListRepo> { VehicleListRepoImpl(get(), get(), get()) }
        factory {VehicleMapper()}
    }
}