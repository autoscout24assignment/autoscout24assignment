package com.datasolutions.data.coroutine

import kotlin.coroutines.CoroutineContext

interface DefaultCorotuine {

    fun Main(): CoroutineContext
    fun IO(): CoroutineContext
    fun Default(): CoroutineContext
}