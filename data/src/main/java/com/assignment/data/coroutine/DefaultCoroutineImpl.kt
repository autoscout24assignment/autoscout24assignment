package com.assignment.data.coroutine

import com.datasolutions.data.coroutine.DefaultCorotuine
import kotlinx.coroutines.Dispatchers
import kotlin.coroutines.CoroutineContext


class DefaultCoroutineImpl: DefaultCorotuine {

    override fun Main(): CoroutineContext {
        return Dispatchers.Main
    }

    override fun IO(): CoroutineContext {
        return Dispatchers.IO
    }

    override fun Default(): CoroutineContext {
        return Dispatchers.Default
    }

}