package com.assignment.data.datasource.remote.response

data class BaseResponse<T>(val result: T)

