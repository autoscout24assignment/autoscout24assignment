package com.assignment.data.datasource.remote

import com.assignment.data.config.Configuration.VEHICLE_LIST_URL
import com.assignment.data.config.Configuration.VEHICLE_NOTES_URL
import com.assignment.data.datasource.remote.response.BaseResponse
import com.assignment.data.datasource.remote.response.dto.VehicleDto
import com.assignment.data.datasource.remote.response.dto.VehicleNoteDto

import retrofit2.http.*

interface ApiService {


    @GET (VEHICLE_LIST_URL)
    suspend fun getVehicles(): List<VehicleDto>

    @GET (VEHICLE_NOTES_URL)
    suspend fun getVehiclesNotes(): List<VehicleNoteDto>

}