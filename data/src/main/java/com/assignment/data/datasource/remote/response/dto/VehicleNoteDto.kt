package com.assignment.data.datasource.remote.response.dto

data class VehicleNoteDto(val vehicleId: Int, val note: String? = null)
