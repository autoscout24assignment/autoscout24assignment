package com.assignment.data.datasource.remote.response.dto

data class VehicleDto(
    val id: Int,
    val make: String? = null,
    val model: String? = null,
    val fuel: String? = null,
    val price: Int? = null,
    var comment: String? = null,
    val images: List<Image>? = null
)

data class Image(val url: String)
