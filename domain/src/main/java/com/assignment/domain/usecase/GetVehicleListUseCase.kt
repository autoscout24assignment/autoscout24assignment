package com.assignment.domain.usecase

import com.assignment.domain.repository.VehicleListRepo

class GetVehicleListUseCase(private val vehicleListRepo: VehicleListRepo) {
    suspend fun invoke() =
        vehicleListRepo.getVehicles()
}