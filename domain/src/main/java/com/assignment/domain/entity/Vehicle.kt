package com.assignment.domain.entity

data class Vehicle(
    val id: Int,
    val title: String,
    val fuelType: String,
    val price: String,
    val comment: String,
    val images: List<String>
)
