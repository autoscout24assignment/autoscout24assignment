package com.assignment.domain.entity

data class VehicleNote(val vehicleId: Int, val note: String)
