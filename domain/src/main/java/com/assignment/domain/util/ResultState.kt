package com.assignment.domain.util

sealed class ResultState<out T> {
    class Success<out T>(val data: T): ResultState<T>()
    object loading: ResultState<Nothing>()
    object default: ResultState<Nothing>()
    class Error(val message: String): ResultState<Nothing>()
}