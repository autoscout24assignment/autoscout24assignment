package com.assignment.domain.di

import com.assignment.domain.usecase.GetVehicleListUseCase
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module

object UsecaseModules {

    fun load() {
        loadKoinModules(usecaseModules)
    }

    private val usecaseModules = module {
        factory { GetVehicleListUseCase(get()) }
    }
}