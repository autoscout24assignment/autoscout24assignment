package com.assignment.domain.repository

import com.assignment.domain.entity.Vehicle
import com.assignment.domain.entity.VehicleNote
import com.assignment.domain.util.ResultState
import kotlinx.coroutines.flow.Flow


interface VehicleListRepo {
    suspend fun getVehicles(): Flow<ResultState<List<Vehicle>>>
}