package com.assignment.core.di

import com.assignment.core.config.Configurations
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitModule {

    fun load() {
        loadKoinModules(retrofitModule)
    }

    private val retrofitModule = module {

        single {
            Retrofit.Builder().client(get())
                .addConverterFactory(gsonConverterFactory())
                .baseUrl(get<String>())
                .build() as Retrofit
        }
        single { Configurations.BASE_URL }
    }

    private fun gsonConverterFactory(): GsonConverterFactory {
        return GsonConverterFactory.create()
    }
}