package com.assignment.core.config


object Configurations {


    const val BASE_URL = "http://localhost/"
    const val CALL_TIMEOUT: Long = 60
    const val CONNECT_TIMEOUT: Long = 60
    const val READ_TIMEOUT: Long = 60
}