package com.assignment.core.util

import androidx.navigation.NavDirections

sealed class NavigationCommand {
    data class To(val direction: NavDirections): NavigationCommand()
    object Back: NavigationCommand()
    data class BackTo(val destinationId: Int): NavigationCommand()
}
