package com.assignment.core.base.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.assignment.core.util.NavigationCommand


abstract class BaseViewModel : ViewModel() {

    val loadingEvent = MutableLiveData<Boolean>()

    val errorEvent= MutableLiveData<String>()

    val navigationCommands = MutableLiveData<NavigationCommand>()

    var _navigationEvent = MutableLiveData<NavigationCommand>()
    val navigationEvent: LiveData<NavigationCommand> = _navigationEvent

    fun backButtonClick() {
        navigationCommands.value = NavigationCommand.Back
    }
}