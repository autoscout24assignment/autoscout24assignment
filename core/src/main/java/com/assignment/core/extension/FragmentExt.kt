package com.assignment.core.extension

import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModel
import com.assignment.core.base.fragment.BaseFragment
import org.koin.androidx.viewmodel.ext.android.getViewModel

inline fun <reified T : ViewModel, reified K : ViewDataBinding> BaseFragment<T, K>.injectViewModel(): T {
    return getViewModel(clazz = viewModelClass)
}