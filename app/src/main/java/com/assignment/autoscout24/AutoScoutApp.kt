package com.assignment.autoscout24

import android.app.Application
import com.assignment.autoscout24.di.ApplicationComponents.loadAllComponents
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class AutoScoutApp: Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@AutoScoutApp)
            androidLogger(Level.INFO)
        }

        loadAllComponents()
    }
}