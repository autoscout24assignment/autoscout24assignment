package com.assignment.autoscout24.di

import com.assignment.core.di.OKHttpModule
import com.assignment.core.di.RetrofitModule
import com.assignment.data.di.ApiServiceModule
import com.assignment.data.di.CoroutineModule
import com.assignment.data.di.RepositoryModule
import com.assignment.domain.di.UsecaseModules
import com.assignment.vehiclelist.di.VehicleListModules


object ApplicationComponents {
    fun loadAllComponents() {
        OKHttpModule.load()
        RetrofitModule.load()
        ApiServiceModule.load()
        RepositoryModule.load()
        UsecaseModules.load()
        CoroutineModule.load()
        VehicleListModules.load()
    }
}