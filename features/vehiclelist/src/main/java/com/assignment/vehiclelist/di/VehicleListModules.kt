package com.assignment.vehiclelist.di

import com.assignment.vehiclelist.ui.VehicleListViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module

object VehicleListModules {
    fun load() {
        loadKoinModules(viewModels)
    }

    private val viewModels = module {
        viewModel { VehicleListViewModel(get()) }
    }

}