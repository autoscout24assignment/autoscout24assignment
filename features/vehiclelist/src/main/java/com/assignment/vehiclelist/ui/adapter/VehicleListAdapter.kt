package com.assignment.vehiclelist.ui.adapter


import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.assignment.domain.entity.Vehicle

class VehicleListAdapter(private val onVehicleItemClickListener: OnVehicleItemClickListener): ListAdapter<Vehicle, VehicleListItemViewHolder>(diffUtilCallback) {

    companion object {
        val diffUtilCallback = object: DiffUtil.ItemCallback<Vehicle>() {
            override fun areItemsTheSame(oldItem: Vehicle, newItem: Vehicle): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Vehicle, newItem: Vehicle): Boolean {
               return oldItem == newItem
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VehicleListItemViewHolder {
        return VehicleListItemViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: VehicleListItemViewHolder, position: Int) {
        holder.bind(onVehicleItemClickListener, getItem(position))
    }

    interface OnVehicleItemClickListener {
        fun showDetails(images: List<String>)
    }
}