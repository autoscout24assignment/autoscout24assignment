package com.assignment.vehiclelist.ui

import android.widget.ImageView
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.assignment.core.base.fragment.BaseFragment
import com.assignment.core.extension.injectViewModel
import com.assignment.domain.util.ResultState
import com.assignment.vehiclelist.R
import com.assignment.vehiclelist.BR
import com.assignment.vehiclelist.databinding.FragmentVehicleListBinding
import com.assignment.vehiclelist.ui.adapter.VehicleListAdapter
import com.kodmap.app.library.PopopDialogBuilder
import kotlinx.coroutines.flow.collect
import kotlin.reflect.KClass

class VehicleListFragment: BaseFragment<VehicleListViewModel, FragmentVehicleListBinding>(),
    VehicleListAdapter.OnVehicleItemClickListener {

    private val adapter: VehicleListAdapter by lazy { VehicleListAdapter(this) }

    override val viewModelClass: KClass<VehicleListViewModel>
        get() = VehicleListViewModel::class

    override val layoutId: Int
        get() = R.layout.fragment_vehicle_list

    override val bindingVariable: Int
        get() = BR.viewModel

    override fun registerOtherObservers() {
        lifecycleScope.launchWhenCreated {
            viewModel.vehicleStateFlow.collect { result ->

                when (result) {
                    is ResultState.loading -> {
                        viewModel.showLoading(true)
                    }

                    is ResultState.Error -> {
                        viewModel.showLoading(false)
                        viewModel.showError(result.message)
                    }

                    is ResultState.Success -> {
                        viewModel.showLoading(false)
                        if (result.data.isEmpty())
                            viewModel.showError("No vehicles found.")
                        else {
                            adapter.submitList(result.data)
                        }
                    }
                }
            }
        }
    }

    override fun viewModel()= injectViewModel()

    override fun initViews() {
        setupRecyclerView()
    }

    private fun setupRecyclerView() {
        val layoutManager =
            LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
        binding.rvVehicles.layoutManager = layoutManager
        binding.rvVehicles.adapter = adapter

    }

    override fun showDetails(images: List<String>) {
        val imageSliderDialog = PopopDialogBuilder(requireContext())
            .setList(images)
            .setHeaderBackgroundColor(R.color.purple_500)
            .setDialogBackgroundColor(R.color.color_dialog_bg)
            .setCloseDrawable(R.drawable.ic_close_white_24dp)
            .setIsZoomable(true)
            .setSliderImageScaleType(ImageView.ScaleType.CENTER_INSIDE)
            .build()

        imageSliderDialog.show()
    }
}