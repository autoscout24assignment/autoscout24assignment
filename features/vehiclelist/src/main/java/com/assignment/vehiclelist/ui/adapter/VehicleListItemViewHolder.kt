package com.assignment.vehiclelist.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.assignment.domain.entity.Vehicle
import com.assignment.vehiclelist.R
import com.assignment.vehiclelist.databinding.AdapterItemVehicleBinding
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions


class VehicleListItemViewHolder(private val binding: AdapterItemVehicleBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(
        onVehicleItemClickListener: VehicleListAdapter.OnVehicleItemClickListener,
        vehicle: Vehicle
    ) {
        binding.vehicle = vehicle
        if (vehicle.images.isNotEmpty()) {
            loadImage(vehicle.images[0])
            binding.root.setOnClickListener {
                onVehicleItemClickListener.showDetails(vehicle.images)
            }
        }

        binding.executePendingBindings()
    }

    private fun loadImage(url: String) {
        Glide.with(binding.root)
            .load(url)
            .error(R.drawable.ic_place_holder)
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(binding.carImage)
    }

    companion object {
        fun create(parent: ViewGroup): VehicleListItemViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding = AdapterItemVehicleBinding.inflate(layoutInflater, parent, false)
            return VehicleListItemViewHolder(binding)
        }
    }
}