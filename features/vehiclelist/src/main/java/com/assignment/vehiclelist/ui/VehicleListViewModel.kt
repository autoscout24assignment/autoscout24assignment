package com.assignment.vehiclelist.ui

import androidx.lifecycle.viewModelScope
import com.assignment.core.base.viewmodel.BaseViewModel
import com.assignment.domain.entity.Vehicle
import com.assignment.domain.entity.VehicleNote
import com.assignment.domain.usecase.GetVehicleListUseCase
import com.assignment.domain.util.ResultState
import com.datasolutions.data.coroutine.DefaultCorotuine
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class VehicleListViewModel(val getVehicleListUseCase: GetVehicleListUseCase
): BaseViewModel() {


    private val _vehicleStateFlow =
        MutableStateFlow<ResultState<List<Vehicle>>>(ResultState.loading)
    val vehicleStateFlow: StateFlow<ResultState<List<Vehicle>>> = _vehicleStateFlow

    init {
        getVehicles()
    }

    private fun getVehicles() {
        viewModelScope.launch {
            getVehicleListUseCase.invoke().collect { results ->
                _vehicleStateFlow.value = results
            }
        }
    }


    fun showError(message: String) {
        errorEvent.value = message
    }

    fun showLoading(show: Boolean) {
        loadingEvent.value = show
    }
}